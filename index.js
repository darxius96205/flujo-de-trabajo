const express = require('express');
const app = express();

app.set('port', 3000);

app.get('/', (req, res) => {
    res.send('Hola Banco de Bogotá');
});


app.listen(app.get('port'), () => console.log("server on port", app.get('port')));